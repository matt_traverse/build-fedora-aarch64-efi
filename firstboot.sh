#!/bin/sh

ROOT_DEVICE_MAJOR_MINOR=$(stat -c '%d' /)
ROOT_DEVICE_MAJOR=$(($ROOT_DEVICE_MAJOR_MINOR>>8))
ROOT_DEVICE_MINOR=$(($ROOT_DEVICE_MAJOR_MINOR&255))
source /sys/dev/block/${ROOT_DEVICE_MAJOR}:0/uevent
echo "${DEVNAME}"
growpart "/dev/${DEVNAME}" "${ROOT_DEVICE_MINOR}"
resize2fs "/dev/${DEVNAME}${ROOT_DEVICE_MINOR}"
