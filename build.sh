#!/usr/bin/env bash
set -e
SCRIPTPATH=$(readlink -f "$0")
THISDIR=$(dirname "$SCRIPTPATH")

FEDORA_RELEASE_VER=${FEDORA_RELEASE_VER:-28}
FEDORA_REPO=${FEDORA_REPO:-fedora}

if [[ $(id -u) -ne 0 ]]; then
	echo "Please run this script with superuser privledges"
	exit
fi

if [ ! -x "$(command -v pwgen)" ] && [ -z ${ROOT_PASSWORD+x} ]; then
	echo -n "pwgen is needed to generate a random root password, unless one is supplied with "
	echo "ROOT_PASSWORD"
	exit
fi

ROOT_PASSWORD=${ROOT_PASSWORD:-`pwgen 16 1`}

sleep 5

modprobe nbd max_part=8
qemu-img create -f qcow2 fedora.qcow2 5G
qemu-nbd -c /dev/nbd0 fedora.qcow2
parted --script /dev/nbd0 \
    mklabel gpt \
    mkpart efi fat32 0% 140MiB \
    mkpart boot ext4 140MiB 384MiB \
    mkpart swap linux-swap 384MiB 2304MiB \
    mkpart system ext4 2304MiB 100% \
    set 1 boot on \
    set 1 esp on 

partx -a /dev/nbd0 || echo "partx failed - you can ignore this"
partprobe /dev/nbd0
mkfs.fat -F 32 -s 2 /dev/nbd0p1 

mkfs.ext4 /dev/nbd0p2 
mkswap /dev/nbd0p3
mkfs.ext4 /dev/nbd0p4
mkdir -p /mnt/fedora/
mount /dev/nbd0p4 /mnt/fedora/

mkdir -p /mnt/fedora/boot/

mount /dev/nbd0p2 /mnt/fedora/boot/
mkdir /mnt/fedora/boot/efi/
mount /dev/nbd0p1 /mnt/fedora/boot/efi/
sleep 5

yum -y --disablerepo="*" --exclude="kernel*" --enablerepo="${FEDORA_REPO}" --releasever="${FEDORA_RELEASE_VER}" --installroot=/mnt/fedora groupinstall core
mount --bind /dev /mnt/fedora/dev/
mount --bind /sys /mnt/fedora/sys/
mount -t proc none /mnt/fedora/proc/
cp /etc/resolv.conf /mnt/fedora/etc/

sed -i '13iexclude=kernel*' /mnt/fedora/etc/yum.repos.d/fedora.repo
chroot /mnt/fedora /bin/sh -c "yum -y --releasever=\"${FEDORA_RELEASE_VER}\" install fedora-release"
chroot /mnt/fedora /bin/sh -c "dnf -y group install 'Minimal Install'"

cp traverse-kernel.repo /mnt/fedora/etc/yum.repos.d/
cp dracut-module-ls1043-usb-boot /mnt/fedora/etc/dracut.conf.d/10-ls1043-usb-boot.conf

chroot /mnt/fedora /bin/sh -c "yum -y install kernel"

KERNEL_VERSION=$(ls /mnt/fedora/lib/modules)
echo "Rebuildining initramfs for ${KERNEL_VERSION}"
# Ensure the required modules (dwc3 etc.) are in initrd
chroot /mnt/fedora /bin/bash --login -c "dracut -f /boot/initramfs-${KERNEL_VERSION}.img ${KERNEL_VERSION}"
# Check if the DWC modules are there
chroot /mnt/fedora /bin/bash --login -c "lsinitrd /boot/initramfs-${KERNEL_VERSION}.img | grep dwc"

chroot /mnt/fedora /bin/sh -c "yum -y install grub2 cloud-utils-growpart"

#chroot /mnt/fedora /bin/sh -c 'grub-install --efi-directory=/boot --no-nvram --removable'

if [ ! -d /mnt/fedora/etc/default/ ]; then
	mkdir -p /mnt/fedora/etc/default/
fi

ROOT_DEVICE=`blkid /dev/nbd0p4 -o value -s PARTUUID`
cat <<EOF > /mnt/fedora/etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="console=ttyS0,115200 rootwait net.ifnames=0"
GRUB_DEVICE_UUID=${ROOT_DEVICE}
GRUB_TERMINAL="serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
EOF

#chroot /mnt/fedora /bin/sh -c 'update-grub'
chroot /mnt/fedora /bin/sh -c "grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg"

# As grub doesn't want to generate with a UUID (even when specified, force it)
sed -i "s/root=\/dev\/nbd0p4/root=PARTUUID=${ROOT_DEVICE}/g" /mnt/fedora/boot/efi/EFI/fedora/grub.cfg
grep "PARTUUID=${ROOT_DEVICE}" /mnt/fedora/boot/efi/EFI/fedora/grub.cfg
if [[ "$?" -ne 0 ]]; then
	echo "ERROR: PARTUUID is not in the generated grub.cfg, the script may need to be adjusted"
	echo "NOTE: /mnt/fedora is still mounted for debugging purposes"
	exit
fi

# Fedora installs its GRUB EFI into EFI/fedora/grubaa64.efi, and on a real system,
# expects EFI variables to point to it. Here, we need to act like we are on removable media
mkdir /mnt/fedora/boot/efi/EFI/boot/
cp /mnt/fedora/boot/efi/EFI/fedora/grubaa64.efi /mnt/fedora/boot/efi/EFI/boot/bootaa64.efi
echo "fedora-arm64" > /mnt/fedora/etc/hostname

#cat <<EOF > /mnt/fedora/etc/network/interfaces.d/eth0
#auto eth0
#iface eth0 inet dhcp
#EOF

echo "root:${ROOT_PASSWORD}" | chroot /mnt/fedora chpasswd
echo "*** root password is ${ROOT_PASSWORD} ***"
chroot /mnt/fedora /bin/sh -c 'depmod -a $(ls /lib/modules/)' || : # depmod complains about the running kernel not being there

BOOT_DEVICE=$(blkid /dev/nbd0p2 -o value -s PARTUUID)
EFI_DEVICE=$(blkid /dev/nbd0p1 -o value -s PARTUUID)
cat <<EOF > /mnt/fedora/etc/fstab
PARTUUID=${ROOT_DEVICE}		/		ext4	defaults	1	1
PARTUUID=${BOOT_DEVICE}		/boot	ext4	defaults	1	1
PARTUUID=${EFI_DEVICE}		/boot/efi vfat defaults	1	1
EOF

# gpg-agent seems to hang around, kill it
chroot /mnt/fedora /bin/sh -c "killall gpg-agent" || :

# Make sure we boot to console
chroot /mnt/fedora /bin/sh -c "systemctl set-default multi-user.target"

# Disable irqbalance (if present)  - causes issues with DPAA Ethernet on LS1043
chroot /mnt/fedora /bin/sh -c "systemctl disable irqbalance" || :

# Make sure /var/run is symlinked correctly, or the systemd-dbus does not work
chroot /mnt/fedora /bin/sh -c "rm -rf /var/run; cd /var/; ln -s /run run"

# Copy the first boot
cp firstboot.sh /mnt/fedora/usr/sbin/firstboot.sh
cp rc.local /mnt/fedora/etc/rc.d/rc.local

 # Teardown
umount -R /mnt/fedora
qemu-nbd -d /dev/nbd0
echo $?
if [[ "$?" -eq 0 ]]; then
       qemu-nbd -d /dev/nbd0
fi
