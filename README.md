# Fedora ARM64 EFI (GRUB2) image for Traverse boards and KVM

_To build a CentOS image, checkout the 'centos' branch of this repository_

This script builds a Fedora image with customizations for Traverse ARM boards, as well
as under KVM (aarch64) virtualization.

## Requirements
- Native ARM64/aarch64 host (qemu-user support TBD)
- Network Block Device (nbd) support 
- yum
- pwgen
- qemu-nbd and qemu-img

## Usage
First, ensure that yum (on the host system) has a repo defined, for example:
```
# cat /etc/yum/repos.d/fedora.repo
[fedora]
name=fedora
#baseurl=http://mirror.aarnet.edu.au/pub/fedora/linux/development/28/Everything/aarch64/os/
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-28&arch=aarch64
repo_gpgcheck=0
enabled=0
type=rpm
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-28-primary
```

You might need to import the GPG key for the desired release:
```
curl https://pagure.io/fedora-repos/raw/f27/f/RPM-GPG-KEY-fedora-28-primary -O
rpm --import RPM-GPG-KEY-fedora-28-primary
```

**Important!** SELinux needs to be set to permissive to run this successfully,
otherwise chpasswd invocation fails :(
```
setenforce Permissive
```

If you know of a workaround, please let me know.

```
# Default Fedora CDN mirror, randomly generated password
sudo ./build.sh

# Set root password
sudo ROOT_PASSWORD=hunter2 ./build.sh
```

To copy the generated qcow2 image to a real device:
```
qemu-img convert -f qcow2 -O raw fedora.qcow2 /dev/sdb
```

To launch a VM:
```
./startvm.sh
```

A Tianocore EFI image (QEMU_EFI.fd) is required, this can be obtained from the 
[Linaro Snapshot server](http://snapshots.linaro.org/components/kernel/leg-virt-tianocore-edk2-upstream/latest/QEMU-AARCH64/RELEASE_GCC5/)

## Tested Platforms
- QEMU/KVM (native virtualization) with Linaro Tianocore BIOS, on NXP LS1043A hardware
- Traverse LS1043 boards (V and S).

## Known issues
- Deterministic network interface names are disabled until I can figure out how best to do
this on bare metal without using ACPI, as well as setting up the DHCP configuration on boot.

## See also
[Alpine Linux image generator](https://gitlab.com/mcbridematt/build-alpine-aarch64-efi)

[Debian image generator](https://gitlab.com/matt_traverse/build-debian-aarch64-efi)
